stages:
  - deploy
tags:
    - docker  

variables:
  KUBE_NAMESPACE: "default"
  KUBE_CONFIG: "$CI_PROJECT_DIR/kubeconfig.yaml"
  HELM_RELEASE_NAME: "docker-nginx"
  HELM_CHART_PATH: "/cicd_runner/cicd-helm"

before_script:
  - mkdir -p $CI_PROJECT_DIR/.kube
  - echo "$KUBE_CONFIG_DATA" | base64 -d > $CI_PROJECT_DIR/kubeconfig.yaml
  - kubectl config set-context $(kubectl config current-context) --namespace=$KUBE_NAMESPACE

deploy:
  stage: deploy
  script:
    - helm upgrade --install $HELM_RELEASE_NAME $HELM_CHART_PATH
